import supertest from 'supertest';
import { token, urls } from '../config';

export const MailBoxCheck = function () {
  this.get = async function (email, accessKey = null) {
    const r = await supertest(`${urls.mailbox}`)
      .get(`/api/check?access_key=${accessKey || token}&email=${email}`);
    return r;
  };
};
