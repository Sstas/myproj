import { test } from '@jest/globals';
import { MailBoxCheck } from '../framework/controllers/mailboxCheckController';

describe('Tests for mailbox check API', () => {
  it('should check valid email', async () => {
    const r = await new MailBoxCheck().get('bob@hotmail.com');
    expect(r.status)
      .toEqual(200);
    expect(r.body.email)
      .toEqual('bob@hotmail.com');
    expect(r.body.format_valid)
      .toEqual(true);
    expect(r.body.user)
      .toEqual('bob');
    expect(r.body.score)
      .toBeGreaterThan(0);
  });

  test.each([
    ['valid@email.com', true],
    ['  valid@email.com  ', true],
    ['', undefined],
    ['Name Name', false],
    ['  ', undefined],
    ['m@d.com', true],
    ['m@d@c.com', false],
    ['a+b@dom.com', true],
  ])('parameterized test', async (email, checkResult) => {
    const r = await new MailBoxCheck().get(email);
    expect(r.status)
      .toEqual(200);
    expect(r.body.format_valid)
      .toBe(checkResult);
  });

  it('should return error code for unauthorized request', async () => {
    const r = await new MailBoxCheck().get('bob@hotmail.com', 'aaaaa2222290935c490c1b6f737c8eac');
    expect(r.status)
      .toEqual(200);
    expect(r.body.error.code)
      .toEqual(101);
    expect(r.body.error.type)
      .toEqual('invalid_access_key');
    expect(r.body.success)
      .toEqual(false);
  });
});
